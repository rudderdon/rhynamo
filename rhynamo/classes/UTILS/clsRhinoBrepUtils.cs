﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Rhino.FileIO;
using Rhino.Geometry;
using Rhino.Geometry.Collections;
using Rhino.Collections;

namespace Rhynamo
{
  class clsRhinoBrepUtils
  {
    /// <summary>
    /// Sorted 2D Curve Loops (first curve is the "outer" curve)
    /// </summary>
    public List<Rhino.Geometry.PolyCurve> Curve2DLoops;

    /// <summary>
    /// Sorted 3D Curve Loops (first curve is the "outer" curve)
    /// </summary>
    public List<Rhino.Geometry.PolyCurve> Curve3DLoops;

    /// <summary>
    /// All 2D Segments
    /// </summary>
    public List<Rhino.Geometry.Curve> Curve2DSegments
    {
      get
      {
        List<Rhino.Geometry.Curve> crvs = new List<Rhino.Geometry.Curve>();
        for (int i = 0; i < Curve2DLoops.Count; i++)
        {
          Rhino.Geometry.PolyCurve c = Curve2DLoops[i];
          for (int j = 0; j < c.SegmentCount; j++)
          {
            crvs.Add(c.SegmentCurve(j));
          }
        }
        return crvs;
      }
    }

    /// <summary>
    /// All 3D segments
    /// </summary>
    public List<Rhino.Geometry.Curve> Curve3DSegments
    {
      get
      {
        List<Rhino.Geometry.Curve> crvs = new List<Rhino.Geometry.Curve>();
        for (int i = 0; i < Curve3DLoops.Count; i++)
        {
          Rhino.Geometry.PolyCurve c = Curve3DLoops[i];
          for (int j = 0; j < c.SegmentCount; j++)
          {
            crvs.Add(c.SegmentCurve(j));
          }
        }
        return crvs;
      }
    }

    /// <summary>
    /// A class for sorting brep edges
    /// </summary>
    /// <param name="rh_2d">2D loops</param>
    /// <param name="rh_3d">3D loops</param>
    public clsRhinoBrepUtils(List<Rhino.Geometry.PolyCurve> rh_2d, List<Rhino.Geometry.PolyCurve> rh_3d, Rhino.Geometry.NurbsSurface rh_srf)
    {
      //widen
      InnerOuterSort(rh_2d, rh_3d);
      //LoopDirCheck(rh_srf);
    }

    /// <summary>
    /// Sorts based on inner or outer condition.  the first curve is the outer curve.
    /// </summary>
    /// <param name="rh_2dpc"></param>
    /// <param name="rh_3dpc"></param>
    private void InnerOuterSort(List<Rhino.Geometry.PolyCurve> rh_2dpc, List<Rhino.Geometry.PolyCurve> rh_3dpc)
    {

      List<Rhino.Geometry.BoundingBox> rh_boxes = new List<Rhino.Geometry.BoundingBox>();
      List<double> m_boxextents1 = new List<double>();
      List<double> m_boxextents2 = new List<double>();

      // get bounds
      foreach (Rhino.Geometry.PolyCurve c in rh_2dpc)
      {
        Rhino.Geometry.Plane rh_pln = Rhino.Geometry.Plane.WorldXY;
        Rhino.Geometry.BoundingBox rh_box = c.GetBoundingBox(true);
        rh_boxes.Add(rh_box);

        double m_extents = rh_box.Min.DistanceTo(rh_box.Max);
        m_boxextents1.Add(m_extents);
        m_boxextents2.Add(m_extents);
      }

      Rhino.Geometry.PolyCurve[] rh_2darr = rh_2dpc.ToArray();
      Rhino.Geometry.PolyCurve[] rh_3darr = rh_3dpc.ToArray();
      double[] rh_extentsarr1 = m_boxextents1.ToArray();
      double[] rh_extentsarr2 = m_boxextents2.ToArray();

      Array.Sort(rh_extentsarr1, rh_2darr);
      Array.Sort(rh_extentsarr2, rh_3darr);

      //Array.Reverse(rh_2darr);
      //Array.Reverse(rh_3darr);

      Curve2DLoops = rh_2darr.ToList();
      Curve3DLoops = rh_3darr.ToList();
    }

    /// <summary>
    /// Loop direction check
    /// </summary>
    /// <param name="rh_srf"></param>
    private void LoopDirCheck(Rhino.Geometry.NurbsSurface rh_srf)
    {
      Brep rh_brep = rh_srf.ToBrep();
      BrepEdgeList rh_brepedges = rh_brep.Edges;
      Rhino.Geometry.PolyCurve pc = new Rhino.Geometry.PolyCurve();
      foreach (Rhino.Geometry.Curve c in rh_brepedges)
      {
        pc.Append(c);
      }
      Rhino.Geometry.BoundingBox rh_srfbounds = pc.GetBoundingBox(true);
      Rhino.Geometry.BoundingBox rh_loopbounds = Curve3DLoops[Curve2DLoops.Count - 1].GetBoundingBox(true);

      double min = rh_srfbounds.Min.DistanceTo(rh_loopbounds.Min);
      double max = rh_srfbounds.Max.DistanceTo(rh_loopbounds.Max);

      // curve is an outer trim loop... reverse its direction
      if (Math.Round(min, 3) > 0 || Math.Round(max, 3) > 0)
      {
        Curve3DLoops[Curve3DLoops.Count - 1].Reverse();
        Curve2DLoops[Curve2DLoops.Count - 1].Reverse();
      }
    }

    /// <summary>
    /// Flip the direction of the polycurve
    /// </summary>
    /// <param name="ds_pc"></param>
    /// <returns></returns>
    private Rhino.Geometry.PolyCurve FlipCurve(Rhino.Geometry.PolyCurve rh_pc)
    {
      bool rev = rh_pc.Reverse();
      if (rev)
      {
        return rh_pc;
      }
      else
      {
        return null;
      }

    }
  }
}
