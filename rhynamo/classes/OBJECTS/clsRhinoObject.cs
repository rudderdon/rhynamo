﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Rhino.FileIO;
using Rhino.Geometry;

namespace Rhynamo.classes
{
  class clsRhinoObject
  {
    public Object RhinoObject;
    public Rhino.DocObjects.Layer ObjectLayer;
    public string ObjectName;
    public System.Drawing.Color ObjectColor;

    public clsRhinoObject(Object _obj, Rhino.DocObjects.Layer _layer, string _name, System.Drawing.Color _color)
    {
      RhinoObject = _obj;
      ObjectLayer = _layer;
      ObjectName = _name;
      ObjectColor = _color;
    }
  }
}
